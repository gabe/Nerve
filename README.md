# Nerve
## A matrix receptionist bot

## How to get started

1. Register a matrix bot on your homeserver
2. Grab [data-example.json](./data-example.json) and modify it with your bot's credentials 
3. Save as `data.json`
4. Create your own questions & answers and setup your keywords
5. Run the bot `python main.py`

## Docker container
1. `docker build -t matrix-nerve .`
2. `docker run -it matrix-nerve`

You may want to set data.json as a volume to be able to make changes on the fly

## Details

* The bot will listen to `!faq` and post the entire FAQ
![Adding questions from matrix](screenshots/adding-questions.gif)


## Features in the works
* Alerts
* Dynamic configuration
* Meeting requests (through easyappointments)
* Configurable API calls



