import requests,json

class easyappointments:
    def __init__(self,token,url):
        self.token = token
        self.url = url
        self.headers = {"Authorization":f"Bearer {token}"}
        self.appointments = {}
        self.services = {}
        self.providers = {}
        self.customers = {}
        try:
            self.update_data()
        except:
            print("Failed to connect to easyappointments!")

    def get_appointments(self):
        data = requests.get(self.url+'/api/v1/appointments',headers=self.headers)
        return data.json()
    def get_services(self):
        data = requests.get(self.url+'/api/v1/services',headers=self.headers)
        return data.json()
    def get_providers(self):
        data = requests.get(self.url+'/api/v1/providers',headers=self.headers)
        return data.json()
    def get_customers(self):
        data = requests.get(self.url+'/api/v1/customers',headers=self.headers)
        return data.json()

    def get_availabilities(self,service,provider,date):
        try:
            data = requests.get(self.url+f"/api/v1/availabilities?serviceId={service}&providerId={provider}&date={date}",headers=self.headers)
            return data.json()
        except:
            return False          

    def update_data(self):
        appointments = self.get_appointments()
        for a in appointments:
            self.appointments[a['id']] = a
        services = self.get_services()
        for s in services:
            self.services[s['id']]=s
        providers = self.get_providers()
        for p in providers:
            self.providers[p['id']]=p
        customers = self.get_customers()
        for c in customers:
            self.customers[c['id']]=c

    def is_customer(self,mxid):
        customers = self.get_customers()
        for c in customers:
            if mxid in c['notes']:
                return c['id']

        return False

    def is_provider(self,mxid):
        providers = self.get_providers()
        for p in providers:
            if mxid in p['notes']:
                return p['id']
        return -1
    def get_upcoming_appointments(self,mxid):
        #get services & customers
        id = self.is_provider(mxid)
        if id:
            apts = []
            data = self.get_appointments()
            for a in data:
                if a['providerId'] == id:
                    apts.append(a)
            return apts
        else:
            return []


  

    def list_upcoming_appointments(self,mxid):
        apts = self.get_upcoming_appointments(mxid)
        if apts == []:
            return "No meetings recorded."
        output = ""
        for a in apts:
            service = self.services[a['serviceId']]['name']
            customer = self.customers[a['customerId']]['firstName']
            output += f"## {service} with {customer}\nTime:{a['start']}\n"
        return output



    def register_customer(self,customer_data):
        #customer data
        """
        {
            "firstName":"name",
            "lastName":"name",
            "email":"email",
            "phone":"phone"
            "notes":"matrix:mxid"
        }
        """
        try: 
            id = requests.post(self.url+'/api/v1/customers',headers=self.headers,data=json.dumps(customer_data)).json()['id']
            self.update_data()
            return id
        except:
            return False

    def register_appointment(self,appointment_data):
        #appointment data
        """{
            "start":"YY-MM-DD HH:MM:SS",
            "customerId":9,
            "providerId":4,
            "serviceId:4
        }
        """
        print("Registering appointment:")
        print(json.dumps(appointment_data))
        result = requests.post(
        self.url+'/api/v1/appointments',
        headers = self.headers,
        data=json.dumps(appointment_data)
        )
        id = result.json()['id']
        if id:
            return id
        else:
            return False



if __name__ == '__main__':
    cdat = { 
        "firstName":"Test",
        "lastName":"",
        "email":"test@testing.xa",
        "phone":"888-888-8888",
        "notes":"matrix:@lol:testing"
    }
    adat = {
        "start":"2022-09-28 09:00:00",
        "customerId":8,
        "providerId":4,
        "serviceId":2
    }

    easy = easyappointments('secrettoken', 'http://localhost:8787/easyappointments/index.php')
