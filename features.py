import json
import requests
import easyappointments
import datetime
"""
Features 
This is the main API for executing functions with data
"""

def check_date(date):
    try:
        datetime.datetime.strptime(date,"%Y-%m-%d")
    except:
        return False
    return True

class API:
    def __init__(self):
        with open('data.json') as f:
            self.data = json.loads(f.read())
            self.appointments = easyappointments.easyappointments(self.data['easyappointments']['token'], self.data['easyappointments']['url'])
            # Only refresh data when a change is made
    def update_data(self):
        with open('data.json','r') as f:
            self.data = json.loads(f.read()) 

    def save(self):
        with open('data.json','w') as f:
            f.write(json.dumps(self.data,indent=2))
            return True
        return False


    #Administration
    def is_admin(self,handle):
        #return bool
        pass
    def add_admin(self,handle):
        #return bool
        if self.is_admin(handle):
            return True
        else:
            self.data["admins"].append(handle)
            self.save()
            return True
    def remove_admin(self,handle):
        #return bool
        if self.is_admin(handle):
            i = self.data["admins"].index(handle)
            self.data["admins"].pop(i)
            self.save()
            return True
        else:
            return True
    
    
    #state management
    def get_user_state(self,handle):
        if handle not in self.data["users"]:
            self.data["users"][handle]={"state":[]}
            self.save()
        return self.data["users"][handle]["state"]
    
    def update_user_state(self,handle,state):
        self.update_data()
        current_state = self.get_user_state(handle)
        self.data["users"][handle]["state"] = state
        self.save()
        return True    
    
    
    #FAQ
    def get_faq_string(self):
        faq = self.data["faq"]
        return faq["header"] + "".join(
            ["\n# "+q["question"]+"\n"+q["answer"]+"\n" for q in faq["questions"]]
        ) + "\n"+ faq["footer"]
        
    def get_faq_questions(self):
        self.update_data()
        questions = self.data['faq']['questions']
        response = ""
        index = 0
        for question in questions:
            response += f"[{index}]:{question['question']}\n"
            index += 1
        return response
            
            
        
    def get_header(self):
        self.update_data()
        return self.data["faq"]["header"]
    def set_header(self,hdr):
        self.update_data()
        #return bool
        self.data["faq"]["header"] = hdr
        self.save()
        return True
    def get_questions(self):
        self.update_data()
        #return questions
        return self.data["faq"]["questions"]
    def add_question(self,qtn):
        self.update_data()
        #return bool
        self.data["faq"]["questions"].append(qtn)
        self.save()
        return True
    def remove_question(self,qtn_i):
        self.update_data()
        #return bool
        self.data["faq"]["questions"].pop(qtn_i)
        self.save()
        return True
    def update_question(self,qtn_i,qtn):
        self.update_data()
        #return bool
        self.data["faq"]["questions"][qtn_i]=qtn
        return True
    
    #Meetings
    def get_meetings(self,mxid):
        self.update_data()
        self.appointments.list_upcoming_appointments(mxid)
        pass
    def list_services(self):
        msg = "## Services:"
        for i in self.appointments.services:
            service = self.appointments.services[i]
            name = service['name']
            price = str(service['price']) + service['currency']
            description = service['description']
            msg += f"\n### {name}\nPrice:{price}\n{description}"
        return msg

    
    def select_service(self):
        msg = "## Choose a service:"
        counter = 1
        for i in self.appointments.services:
            service = self.appointments.services[i]
            name = f"#{counter}: " + service['name']
            price = str(service['price']) + service['currency']
            description = service['description']
            msg += f"\n### {name}\nPrice:{price}\n{description}"
            counter += 1
        msg += "\nPlease enter the # of the service:"
        return msg

    def select_provider(self):
        msg = "## Choose a provider:"
        counter = 1
        for p in self.appointments.get_providers():
            name = p['firstName']+" "+p['lastName']
            msg += f"\n* #{counter}: {name}"
            counter += 1
        msg += "\nPlease enter the # of the provider"
        return msg



    def select_times(self,service,provider,date):
        data = self.appointments.get_availabilities(service, provider, date)
        if data == False or check_date(date) == False:
            return False
        msg = f"## Choose a time for {date}:\n"
        counter = 1
        for t in data:
            msg += f"{counter}){t} "
            counter +=1
        msg +="\nPlease enter the # of time"
        return msg




    def request_meeting(self):
        self.update_data()
        pass
    def accept_meeting(self):
        self.update_data()
        pass
    
    
if __name__ == '__main__':
    api = API()
    print("Storage loaded.\n",api.data['username'])
