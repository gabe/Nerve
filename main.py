import json
import nio
import simplematrixbotlib as botlib
import commands

data = json.loads(open('data.json').read())
creds = botlib.Creds(
    data["homeserver"],
    data['username'],
    data['password']
)
bot = botlib.Bot(creds)

@bot.listener.on_message_event
async def message(room, message):
    try:
        result = commands.handle_command(message.sender, message.body)
        if result['response'] != None and result['response'] != "":
            await bot.api.send_markdown_message(
                room.room_id,
                result['response']
            )
    except:
        with open("error.log",'a') as f:
            f.write(f"Error:{message}")

@bot.listener.on_custom_event(nio.InviteMemberEvent)
async def example(room, event):
    if event.membership == "join":
        await bot.api.send_markdown_message(
            room.room_id,
            commands.get_faq_string()
        )

bot.run()
